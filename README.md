## Image Data Explorer

Most bioimaging projects derive data from images and regions of interest (ROIs, e.g. segmented objects) on those images. It is desirable, and sometimes necessary, to explore these image-derived data while visualizing the image(s) and ROIs associated with each data point. To address this need, we developed the Image Data Explorer (IDE). The IDE is implemented as a Shiny app (i.e. a web app written in R using the Shiny package).

If you use it, please cite: Muller C, Serrano-Solano B, Sun Y, Tischer C, Hériché J-K (2022) The Image Data Explorer: Interactive exploration of image-derived data. PLoS ONE 17(9): e0273698. https://doi.org/10.1371/journal.pone.0273698

### Quick start
Before you start, make sure that your data conforms to the requirements described in the [wiki section on preparing data for use with the IDE](https://git.embl.de/heriche/image-data-explorer/-/wikis/Preparing-the-data-for-use-with-the-IDE).  


Don't want to install anything? Try our [ready-to-use cloud-based instance](https://shiny-portal.embl.de/shinyapps/app/01_image-data-explorer). 
Note that when using cloud-based instances, images can only be accessed if they are in an S3-compatible object store.  

Check our [2 min video introduction](videos/intro.mp4).  

The IDE needs a computer with an [R environment version >=4.3.0](https://www.r-project.org/). The IDE requires some R packages and will try to install them if it can't detect them on the system. For more details, check the [wiki section on installation](https://git.embl.de/heriche/image-data-explorer/-/wikis/Installation).

Download the code from the project's repository and run it from within the project directory:
```
git clone git@git.embl.de:heriche/image-data-explorer.git
cd image-data-explorer
R -e 'shiny::runApp("image_data_explorer.R", host = "0.0.0.0", port = 5476)'
```
The app is then accessible from a web browser at http://127.0.0.1:5476  
Alternatively, open the file image_data_explorer.R with [RStudio](https://www.rstudio.com/products/rstudio/download/) then click the 'Run App' button.

The app can also be [run in a container](https://git.embl.de/heriche/image-data-explorer/-/wikis/Installation#building-and-running-the-app-in-a-container).

More details can be found in the [wiki](https://git.embl.de/heriche/image-data-explorer/-/wikis/home#-user-manual).


### Screenshot

<img src="screenshots/image_data_explorer_v1.1.screenshot.png"/>

