#!/usr/bin/env Rscript

cran.mirrors <- c('https://cloud.r-project.org/', 'https://ftp.gwdg.de/pub/misc/cran/')
pkg <- c("configr", "shiny")
new.pkg <- pkg[!(pkg %in% installed.packages())]
if (length(new.pkg)) {
  message(paste0("Installing ", new.pkg, "\n"))  
  install.packages(new.pkg, repos = cran.mirrors)
}

library(configr)
library(shiny)

args = commandArgs(trailingOnly = FALSE)
ide.path <- NULL
# Find where the launch script is
# This is expected to be the image data explorer directory
for(i in 1:length(args)) {
  if(grepl("^\\-\\-file=", args[i])) {
    key.val.pair <- strsplit(args[i], "=")
    ide.path <- file.path(dirname(key.val.pair[[1]][2]), "image_data_explorer.R")
  }
}
args = commandArgs(trailingOnly = TRUE)
# Check whether a TOML file has been provided
ide.config <- NULL
if(length(args) < 1 || !is.toml.file(args[1])) {
  warning("No TOML config file provided.")
} else {
  # Read TOML file and populate IDE variables
  ide.config <- read.config(file = args[1])
}
if(is.null(ide.config$data$data_root) || ide.config$data$data_root == "") {
  stop(paste0("data.data_root must be defined in config file ", args[1]))
}
if(is.null(ide.config$data$table_path) || ide.config$data$table_path == "") {
  stop(paste0("data.table_path must be defined in config file ", args[1]))
}

shiny::runApp(ide.path, 
              host = "0.0.0.0", 
              port = 5476,
              launch.browser = TRUE)
