# Install R
#FROM r-base:4.4.0
FROM rocker/r-ver:4.4.0

# Set maintainer/author label
# Overrides label from base image
LABEL org.opencontainers.image.authors="J-K Heriche (heriche@embl.de)"
LABEL org.opencontainers.image.source="https://git.embl.de/heriche/image-data-explorer"
LABEL org.opencontainers.image.licenses="GPL-3"
LABEL org.opencontainers.image.vendor=""

RUN apt update && apt install -y libssl-dev liblzma-dev libbz2-dev libicu-dev libtiff-dev libfftw3-dev libcurl4-openssl-dev libxml2-dev libssh2-1-dev libgit2-dev libfontconfig1-dev libfreetype-dev libharfbuzz-dev libfribidi-dev default-jre default-jdk git

RUN R CMD javareconf

# Create directories to mount eventual data
RUN mkdir -p /data && mkdir -p /g
ENV DATADIR="/data"

# Create user
RUN useradd -m ide -d /home/ide
# Run as user ide to avoid running as root
USER ide

RUN mkdir -p /home/ide/app/image-data-explorer && mkdir -p /home/ide/R/libs && echo 'R_LIBS=/home/ide/R/libs\n' > /home/ide/.Renviron

# Copy the app to the image
COPY image_data_explorer.R /home/ide/app/image-data-explorer/
COPY R /home/ide/app/image-data-explorer/R
COPY www /home/ide/app/image-data-explorer/www
COPY imageViewer /home/ide/app/image-data-explorer/imageViewer

# Install required R packages
RUN R -e "install.packages(c('remotes', 'rversions', 'roxygen2', 'xml2', 'devtools'), repos=c('https://cloud.r-project.org/', 'https://ftp.gwdg.de/pub/misc/cran/')); \
install.packages(c('data.table', 'shiny', 'DT', 'shinyFiles', 'shinycssloaders', 'shinydashboard', 'shinyjs', 'shinyWidgets', 'shinybusy', 'assertthat'), repos=c('https://cloud.r-project.org/', 'https://ftp.gwdg.de/pub/misc/cran/')); \
install.packages(c('ggplot2', 'plotly', 'RANN', 'MASS', 'dbscan', 'uwot', 'xgboost', 'Ckmeans.1d.dp', 'e1071', 'caret', 'RColorBrewer', 'aws.s3', 'configr'), repos=c('https://cloud.r-project.org/', 'https://ftp.gwdg.de/pub/misc/cran/')); \
install.packages('BiocManager', repos=c('https://cloud.r-project.org/', 'https://ftp.gwdg.de/pub/misc/cran/')); \
install.packages(c('rJava', 'RCurl')); BiocManager::install('EBImage'); BiocManager::install('aoles/RBioFormats'); \
BiocManager::install('Rarr'); library(devtools); setwd('/home/ide/app/image-data-explorer/imageViewer'); install(); devtools::install_git('https://git.embl.de/heriche/omerarr')"

# Use older version of RBioFormats if/when installation of latest version is broken
# RUN R -e "devtools::install_github('aoles/RBioFormats@Bio-Formats_6.10.1')"

# IDE is available on port 5476
EXPOSE 5476

# We need to make sure AWS env variables are defined 
# otherwise the R package aws.s3 defaults to us-east-1
# even when region is explicitly set to "" in the code
ENV AWS_S3_ENDPOINT=""
ENV AWS_ACCESS_KEY_ID=""
ENV AWS_SECRET_ACCESS_KEY=""
ENV AWS_DEFAULT_REGION=""

CMD ["R", "-e", "shiny::runApp('/home/ide/app/image-data-explorer/image_data_explorer.R', host = '0.0.0.0', port = 5476)"]
