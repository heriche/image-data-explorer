# Authors: Coralie Muller & Jean-Karim Heriche

##########################
## Module Explore: Plot ##
##########################

# Creation of the plot and the on-hover info table

############### UI ###############

# Scatterplot
ui_explore_plot <- function(id) {
  ns <- NS(id)
  tabPanel( value = "scatterPlot",
    h4('Scatter plot'),
    box( width = 12, align = "left",
         title = "Plot", solidHeader = TRUE, status = "primary",
           plotlyOutput(ns("plot"), height = '500px')
    ) # End box
  )
} # End ui_explore_plot()

# Histogram
ui_explore_hist <- function(id) {
  ns <- NS(id)
  tabPanel( value = "histogram",
            h4('Histogram'),
            box( width = 12, align = "left",
                 title = "Histogram", solidHeader = TRUE, status = "primary",
                 plotlyOutput(ns("hist"), height = '500px')
            ) # End box
  )
} # End ui_explore_hist()

# Boxplot
ui_explore_boxplot <- function(id) {
  ns <- NS(id)
  tabPanel( value = "boxplot",
            h4('Boxplot'),
            box( width = 12, align = "left",
                 title = "Boxplot", solidHeader = TRUE, status = "primary",
                 plotlyOutput(ns("boxplot"), height = '500px')
            ) # End box
  )
} # End ui_explore_boxplot()


############### Server ###############
plot_server <- function(input, output, session, rv) {
  
  ns <- session$ns # Allow to use ns()

  # Scatterplot
  output$plot <- renderPlotly({
    req(rv$data)
    D <- rv$data[rv$currentRows,]
    clusters <- rv$clusters[rv$currentRows]
    selectedCols <- c(NULL, NULL)
    ## Limit selection to two columns otherwise we get a scatterplot matrix
    ## but allow plotting only one variable
    if(length(rv$selectedVar) == 1) {
      selectedCols[2] <- rv$selectedVar
      selectedCols[1] <- "ide.idx"
      ## Reordering based on data table row order
      nr <- length(rv$dataTable_rows_all)
      # D[rv$dataTable_rows_all[1:nr],"ide.idx"] <- c(1:nr)
      D[rv$dataTable_rows_all[1:nr],("ide.idx"):=c(1:nr)]
    } else if(length(rv$selectedVar) == 2) {
      selectedCols <- rv$selectedVar
    }
    # Add reproducible jitter to selected columns
    for(idx in which(colnames(D) %in% selectedCols)) {
      if(is.numeric(D[,..idx])) {
        D[,..idx] <- {set.seed(59034); jitter(D[,..idx])}
      }
    }
    Dunselected <- D
    if(!is.null(rv$selectedRows) && length(rv$selectedRows)>0) {
      Dunselected <- D[-rv$selectedRows,]
      clusters <- clusters[-rv$selectedRows]
    }
    if(length(selectedCols) > 0 && length(selectedCols) <= 2) {
      colours <- "grey"
      p <- ggplot(data = Dunselected, aes(x = .data[[selectedCols[1]]], y = .data[[selectedCols[2]]], key = .data[["ide.id"]])) +
        labs(x = eval(selectedCols[1]), y = eval(selectedCols[2])) +
        coord_cartesian(expand = TRUE)
      if(!is.null(rv$clusters)) {
        idx.to.na <- which(clusters == "" | is.na(clusters) | tolower(clusters) == "none" 
                           | tolower(clusters) == "n/a" | tolower(clusters) == "not annotated")
        if(length(idx.to.na)>0) {
          clusters[idx.to.na] <- "N/A"
        }
        clusters <- factor(clusters[1:nrow(Dunselected)])
        n.clusters <- length(levels(factor(clusters)))
        if(any(levels(clusters)=="N/A")) {
          clusters <- relevel(clusters, "N/A") # Make N/A first level
        }
        if(n.clusters<=8) {
          colours <- c("grey", brewer.pal(n = 8, name = "Set1"))
        } else if(n.clusters<=25) {
          # Use alphabet colour palette without ebony and iron
          colours <- c("grey", "#F0A0FF", "#0075DC", "#993F00", "#4C005C", "#005C31", "#2BCE48",
                       "#FFCC99", "#94FFB5", "#8F7C00", "#9DCC00", "#C20088", "#003380", "#FFA405",
                       "#FFA8BB", "#426600", "#FF0010", "#5EF1F2", "#00998F", "#E0FF66", "#740AFF", 
                       "#990000", "#FFFF80", "#FFE100", "#FF5005")
        } else {
          colours <- rep("grey", n.clusters)
        }
        p <- p + geom_point(data = Dunselected, 
                            aes(colour = as.factor(clusters)),
                            alpha = 0.5) +
          scale_color_manual(name = rv$groupCol, values = colours)
      } else {
        p <- p + geom_point(data = Dunselected, 
                            colour = colours)
      }
     
      if(!is.null(rv$selectedRows) && length(rv$selectedRows)>0) {
        # Selected points
        p <- p + geom_point(data = D[rv$selectedRows,],
                            aes(x = .data[[selectedCols[1]]], y = .data[[selectedCols[2]]], key = .data[["ide.id"]]),
                            colour = "black")
      }
      ggplotly(p, dynamicTicks = TRUE, tooltip = "none", source = "scatterPlot") %>% 
        layout(legend = list(orientation = "v", x = 1, y = 0.95, font = list(size = 12))) %>%
        config(p = ., staticPlot = FALSE, doubleClick = "reset+autosize",  autosizable = TRUE, displayModeBar = TRUE,
               sendData = FALSE, displaylogo = FALSE,
               toImageButtonOptions = list(
                 format = 'svg', # one of png, svg, jpeg, webp
                 filename ='IDE_scatterplot',
                 height = 700,
                 width = 700,
                 scale = 1 # Multiply title/legend/axis/canvas sizes by this factor
               ),
               modeBarButtonsToRemove = c("sendDataToCloud", "hoverCompareCartesian")) %>% # Control Plotly's tool bar
        toWebGL()
    } # End if
  }) # End renderPlotly for scatterplot
  
  # Histogram
  output$hist <- renderPlotly({
    req(rv$data)
    D <- rv$data[rv$currentRows,]
    selectedCols <- rv$selectedVar
    colours <- "grey"
    if(!is.null(rv$clusters)) {
      clusters <- rv$clusters[rv$currentRows]
      idx.to.na <- which(clusters == "" | is.na(clusters) | tolower(clusters) == "none" | tolower(clusters) == "n/a")
      if(length(idx.to.na)>0) {
        clusters[idx.to.na] <- "N/A"
      }
      clusters <- factor(clusters[1:nrow(D)])
      n.clusters <- length(levels(factor(clusters)))
      if(any(levels(clusters)=="N/A")) {
        clusters <- relevel(clusters, "N/A") # Make N/A first level
      }
      if(n.clusters<=8) {
        colours <- c("grey", brewer.pal(n = 8, name = "Set1"))
      } else if(n.clusters<=25) {
        # Use alphabet colour palette without ebony and iron
        colours <- c("grey", "#F0A0FF", "#0075DC", "#993F00", "#4C005C", "#005C31", "#2BCE48",
                     "#FFCC99", "#94FFB5", "#8F7C00", "#9DCC00", "#C20088", "#003380", "#FFA405",
                     "#FFA8BB", "#426600", "#FF0010", "#5EF1F2", "#00998F", "#E0FF66", "#740AFF", 
                     "#990000", "#FFFF80", "#FFE100", "#FF5005")
      } else {
        clusters <- NULL
      }
      p <- ggplot(data = D, aes(x = .data[[selectedCols[1]]], fill = clusters, color = clusters))
      # Heuristic to choose binning
      if(max(D[, get(selectedCols[1])], na.rm = TRUE)>10) { # use binwidth = 1
        p <- p + geom_histogram(position = "identity", binwidth = 1, aes(y = after_stat(count)), alpha = 0.5) +
          scale_color_manual(name = "", values = colours) + scale_fill_manual(name = "", values = colours)
      } else { # make 30 bins
        p <- p + geom_histogram(position = "identity", bins = 30, aes(y = after_stat(count)), alpha = 0.5) +
          scale_color_manual(name = "", values = colours) + scale_fill_manual(name = "", values = colours)
      }
    } else {
      p <- ggplot(data = D, aes(x = .data[[selectedCols[1]]]))
      if(max(D[, get(selectedCols[1])], na.rm = TRUE)>10) { # use binwidth = 1
        p <- p + geom_histogram(position = "identity", binwidth = 1, aes(y = after_stat(count)), color=colours, fill=colours)
      } else { # make 30 bins
        p <- p + geom_histogram(position = "identity", bins = 30, aes(y = after_stat(count)), color=colours, fill=colours)
      }
    }
    p <- p + theme(plot.title = element_blank(), legend.title = element_blank())

    ggplotly(p, dynamicTicks = TRUE, tooltip = "none", source = "histogram") %>% 
      layout(legend = list(orientation = "v", x = 1, y = 0.95, font = list(size = 12))) %>%
      config(p = ., staticPlot = FALSE, doubleClick = "reset+autosize",  autosizable = TRUE, displayModeBar = TRUE,
             sendData = FALSE, displaylogo = FALSE,
             toImageButtonOptions = list(
               format = 'svg', # one of png, svg, jpeg, webp
               filename ='IDE_histogram',
               height = 700,
               width = 700,
               scale = 1 # Multiply title/legend/axis/canvas sizes by this factor
             ),
             modeBarButtonsToRemove = c("sendDataToCloud", "hoverCompareCartesian")) # Control Plotly's tool bar
    
  }) # End renderPlotly for histogram
  
  # Boxplot
  output$boxplot <- renderPlotly({
    req(rv$data)
    D <- rv$data[rv$currentRows,]
    selectedCols <- rv$selectedVar
    colours <- "grey"
    if(!is.null(rv$clusters)) {
      clusters <- rv$clusters[rv$currentRows]
      idx.to.na <- which(clusters == "" | is.na(clusters) | tolower(clusters) == "none" | tolower(clusters) == "n/a")
      if(length(idx.to.na)>0) {
        clusters[idx.to.na] <- "N/A"
      }
      clusters <- factor(clusters[1:nrow(D)])
      n.clusters <- length(levels(factor(clusters)))
      D$clusters <- clusters
      if(any(levels(clusters)=="N/A")) {
        clusters <- relevel(clusters, "N/A") # Make N/A first level
      }
      p <- ggplot(data = D, aes(x = .data[["clusters"]], y = .data[[selectedCols[1]]])) +
        theme(axis.text.x=element_blank(), axis.title.x=element_blank(),  axis.ticks.x=element_blank())
      if(n.clusters<=8) {
        colours <- c("grey", brewer.pal(n = 8, name = "Set1"))
        p <- p + geom_boxplot(aes(fill = .data[["clusters"]], color = .data[["clusters"]])) +
          scale_fill_manual(name = "", values = colours) +
          scale_color_manual(name = "", values = colours)
      } else if(n.clusters<=25) {
        # Use alphabet colour palette without ebony and iron
        colours <- c("grey", "#F0A0FF", "#0075DC", "#993F00", "#4C005C", "#005C31", "#2BCE48",
                     "#FFCC99", "#94FFB5", "#8F7C00", "#9DCC00", "#C20088", "#003380", "#FFA405",
                     "#FFA8BB", "#426600", "#FF0010", "#5EF1F2", "#00998F", "#E0FF66", "#740AFF", 
                     "#990000", "#FFFF80", "#FFE100", "#FF5005")
        p <- p + geom_boxplot(aes(fill = .data[["clusters"]], color = .data[["clusters"]])) +
          scale_fill_manual(name = "", values = colours) +
          scale_color_manual(name = "", values = colours)
      } else {
        p <- p + geom_boxplot(aes(fill = .data[["clusters"]], color = .data[["clusters"]])) +
          scale_fill_manual(name = "", values = rep('#A4A4A4', n.clusters)) +
          scale_color_manual(name = "", values = rep('black', n.clusters))
      }
    } else {
      p <- ggplot(data = D, aes(y = .data[[selectedCols[1]]])) +
        geom_boxplot(color=colours, fill=colours)
    }
    p <- p + theme(plot.title = element_blank(), legend.title = element_blank())
    ggplotly(p, dynamicTicks = TRUE, tooltip = "none", source = "boxplot") %>% 
      layout(legend = list(orientation = "v", x = 1, y = 0.95, font = list(size = 12))) %>%
      config(p = ., staticPlot = FALSE, doubleClick = "reset+autosize",  autosizable = TRUE, displayModeBar = TRUE,
             sendData = FALSE, displaylogo = FALSE,
             toImageButtonOptions = list(
               format = 'svg', # one of png, svg, jpeg, webp
               filename ='IDE_boxplot',
               height = 700,
               width = 700,
               scale = 1 # Multiply title/legend/axis/canvas sizes by this factor
             ),
             modeBarButtonsToRemove = c("sendDataToCloud", "hoverCompareCartesian")) # Control Plotly's tool bar
  }) # End renderPlotly for boxplot
  
  ### Interactions with the plot
  
  ## Get clicked point
  ## This selects it in the data table
  observe({ # click on scatter plot
    req(rv$data)
    if(length(rv$selectedVar) > 0 && length(rv$selectedVar) <= 2) { # Ensure we have a plot
      click <- event_data("plotly_click", priority = "event", source = "scatterPlot")
      if(!is.null(click)) {
        selectRows(rv$proxy, as.numeric(unlist(click$key))) # Set selected point in table
        # Reset event to avoid triggering updates when switching workspaces
        runjs("Shiny.setInputValue('plotly_click-scatterPlot', null);")
      }
    }
  })
  
  observe({
    req(rv$data)
    if(length(rv$selectedVar) > 0 && length(rv$selectedVar) <= 2) { # Ensure we have a plot
      brush <- event_data("plotly_selected", priority = "event", source = "scatterPlot")
      if(!is.null(brush)) {
        selectRows(rv$proxy, as.numeric(unique(unlist(brush$key))))
        # Reset event to avoid triggering updates when switching workspaces
        runjs("Shiny.setInputValue('plotly_selected-scatterPlot', null);")
      }
    }
  })
  
}
